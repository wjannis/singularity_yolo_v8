# singularity_yolo_v8
This repository was created to document the steps taken to create a docker image on a host machine (in this case the internal server of the team) and converting and deploying it on the BSC HPC cluster (the CTE Power cluster). 


## Step by Step

First the docker container was created from the official YOLO v8 [repo]().

```bash
git clone 
cd ultralytics/docker
```

The docker container is built like usual and then an image of it is created
```bash
docker build -t "name:tag" .
docker run -it "name:tag"
```

and then save the image we just created to a .tar file for transfer to the cluster (note: maybe save files inside that were not covered in the creation by the dockerfile. I copied the dataset into it before saving it.)
```bash
docker save "container_id" > image.tar
scp image.tar user@cluster.bsc.es:
```

The conversion from docker to singularity takes quite some while so it makes sense to get a compute node with at least 1 hour runtime. Also a decent amount of disk space is required, so maybe set the right enviremont variables to large temporary disk space. The conversion is happening through the slurm script but the basic steps are:
```bash
# set cache and tmp dir env variables
export SINGULARITY_CACHEDIR=/gpfs/projects/$(GROUP/$(USER)/.singularity/
export SINGULARITY_TMPDIR=/gpfs/projects/$(GROUP/$(USER)/.singularity/

# build the singularity image
singularity build /gpfs/projects/$(GROUP/$(USER)/yolo.sif docker-archive://image.tar
```

Finally run the singularity container with
```bash
singularity exec --nv image_name.sif main.py  # --nv for gpu support, main.py includes the code that should be run inside the container
```

Obviously, this should be embedded into a slurm script and executed on a compute node.

## License
As Yolo v8 is GPL3 this repo is also licensed under GPL3.

## Project status
At the moment, it cannot be confirmed that the image is running in the end. The testing of the image after conversion is still pending.
